package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    Random random = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        // Main game loop
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = userChoice();
            String cpuChoice = randomChoice();
            String selectedChoices = "Human chose " + humanChoice + ", computer chose " + cpuChoice + ".";
            boolean humanWinner = isWinner(humanChoice, cpuChoice);

//             Check who won
            if (humanChoice.equals(cpuChoice)) {
                System.out.println(selectedChoices + " It's a tie");
            } else if (humanWinner) {
                humanScore ++;
                System.out.println(selectedChoices + " Human wins.");
            } else {
                computerScore ++;
                System.out.println(selectedChoices + " Computer wins.");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            if (!continuePlaying()) {
                break;
            }
            roundCounter ++;
        }
        System.out.println("Bye bye :)");
    }

    boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper") && choice2.equals("rock")) {
            return true;
        } else if (choice1.equals("rock") && choice2.equals("scissors")) {
            return true;
        } else if (choice1.equals("scissors") && choice2.equals("paper")) {
            return true;
        } else {
            return false;
        }
    }

    String randomChoice() {
        int selectedIndex = random.nextInt(3);
        String selectedChoice = rpsChoices.get(selectedIndex);
        return selectedChoice;
    }

    boolean continuePlaying() {
        
        while (true) {
            // System.out.println("Continue (y/n)?");
            String userInput = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();

            if (userInput.equals("y")) {
                return true;
            } else if (userInput.equals("n")) {
                return false;
            } else {
                System.out.println("I don't understand " + userInput + ". Try again");
            }
        }
    }

    String userChoice(){
        String selectedChoice = new String();

        while (true) {
            selectedChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            // Validate selection
            if (rpsChoices.contains(selectedChoice)) {
                break;
            } else {
                System.out.println("I do not understand " + selectedChoice + ". Could you try again?");
            }
        }
        return selectedChoice;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
